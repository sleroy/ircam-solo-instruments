Building on decades of expertise and research in acoustics and sound recording, [UVI](https://www.uvi.net/en/) and [IRCAM](https://www.ircam.fr) recruited the finest musicians, artistic directors and engineers available to produce IRCAM Solo Instruments 2. A massive endeavour, years in the making, we're proud to present to you this seamless blend of traditional and experimental techniques. Old world instruments meet new world performers in this deep and profound library exploring the broader spectrum of sounds possible from these beloved instruments.

> - IRCAM Solo Instruments 2 is free for owners of version 1. Available directly in your [UVI Portal application](https://www.uvi.net/uvi-portal)
> - Retail Price: 299€ VAT included
> - [Premium member](https://www.ircam.fr/innovations/abonnements-du-forum/) Price: -10%
> - Only available by downloading
> - [Technical Support](https://support.uvi.net/hc/en-us/categories/200231062-Technical-Support)
> - [Buy](https://shop.ircam.fr/en/partners/70-ircam-solo-instruments.html)
> - [Download IRCAM Solo Instruments 2 Manual](https://uvi.s3.amazonaws.com/SC85-IRCAM_Solo_Instruments_2/IRCAM_Solo_Instruments_2_manual.pdf)
> - [Download Soundbank Installation Guide](http://www.uvi.net/installing_uvi_soundbanks_en.pdf)

With IRCAM Solo Instruments 2 we've modernized this powerful library with numerous feature and QoL enhancements, including improvements to core scripting, sample library, playability, performance controls and expression, and workflow and usability improvements thanks to an all-new user interface.

- Improved scripting engine allows multiple play and expression modes, including legato, portamento, poly legato and poly portamento, now controllable by modwheel
- New Play mode for all stringed instruments, allows user to play instruments with virtual hand positioning for more natural expression
- All-new UI design, improves aesthetics and usability
- New MPE and Poly Aftertouch support
- Improved sample library, delivers cleaner sustains
- Improved presets and key switch groups
- Improved dynamics section
- Improved envelopes (more natural response)
- Improved effects section

![IRCAM Solo Instruments 2](https://forum.ircam.fr/media/uploads/software/IRCAM%20Solo%20Instruments/isi2.png)

## Overview ##
-	Size: 10.59 GB (FLAC lossless encoding, was 16.21 GB in WAV)
-	Content: 403 presets, 19,799 samples
-	Sample Resolution: 44.1 kHz
-	License: 3 activations per license on any combination of machines or iLok dongles

## System requirements ##
-	Runs in [UVI Workstation](https://www.uvi.net/uvi-workstation.html) version 3.0.12+, and [Falcon](https://www.uvi.net/falcon.html) version 2.0.8+
-	[iLok]( https://www.ilok.com/) account (free, dongle not required)
-	Internet connection for the license activation
-	Supported Operating Systems: Mac OS X 10.9 Mavericks to macOS 10.15 Catalina (64-bit) / Windows 8 to Windows 10 (64-bit)
-	11 GB of disk space
-	Hard Drive: 7,200 rpm recommended or Solid State Drive (SSD)
-	4GB RAM (8 GB+ highly recommended for large UVI Soundbanks)

## Compatibility ##
-	Supported Formats: Audio Unit, AAX, VST, Standalone
-	Tested and Certified in: Digital Performer 8+, Pro Tools 11+, Logic Pro X+, Cubase 7+, Nuendo 6+, Ableton Live 8+, Studio One 2+, Garage Band 6+, Maschine 1+, Tracktion 4+, Vienna Ensemble Pro 5+, Reaper 4+, Sonar X3+, MainStage 3, MuLab 5.5+, FL Studio, Bitwig 1+, Reason 9.5+

